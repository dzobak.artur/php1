<?php


$zapravka = [
    'code' => null,
    'adress' => null,
    'firma' => null,
    'price' => [
        'a-95' => null,
        'a-98' => null,
        'dp' => null,
    ],
    'fuel' => [
        'fuel1' => null,
        'fuel2' => null,
        'fuel3' => null,
    ],
    'litre' => null,

];

$zapravkas = [ [


    'code' => 1,
    'adress' => 'Ukraine',
    'firma'=> 'OKKO',
    'price' => [
        'a-95' => 49,
        'a-98' => 51,
        'dp' => 50,
    ],
    'fuel' => [
        'fuel1' => 'a-95',
        'fuel2' => 'a-98',
        'fuel3' => 'dp'
    ],
    'litre' => 300,
],
    [


        'code' => 2,
        'adress' => 'Germany',
        'firma'=> 'RedBull',
        'price' => [
            'a-95' => 48,
            'a-98' => 55,
            'dp' => 41,
        ],
        'fuel' => [
            'fuel1' => 'a-95',
            'fuel2' => 'a-98',
            'fuel3' => 'dp'
        ],
        'litre' => 500,
    ],

    [


        'code' => 3,
        'adress' => 'France',
        'firma'=> 'UPG',
        'price' => [
            'a-95' => 43,
            'a-98' => 50,
            'dp' => 49,
        ],
        'fuel' => [
            'fuel1' => 'a-95',
            'fuel2' => 'a-98',
            'fuel3' => 'dp'
        ],
        'litre' => 900,
    ],

    [


        'code' => 4,
        'adress' => 'Spain',
        'firma'=> 'SHELL',
        'price' => [
            'a-95' => 48,
            'a-98' => 55,
            'dp' => 49,
        ],
        'fuel' => [
            'fuel1' => 'a-95',
            'fuel2' => 'a-98',
            'fuel3' => 'dp'
        ],
        'litre' => 250,
    ],
];
$errors = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $editedIndex = $_POST['edited_index'] ?? -1;
    $editedZapravka = $zapravkas[$editedIndex] ?? null;


    if (empty($_POST['firma'])) {
        $errors['firma'] = 'Поле "Фірма" не може бути порожнім';
    }


    if (!is_numeric($_POST['litre']) || $_POST['litre'] < 0) {
        $errors['litre'] = 'Поле "Літр" повинно бути невід\'ємним числом';
    }

    if (empty($errors)) {
        if ($editedIndex !== -1) {
            $zapravkas[$editedIndex] = [
                'code' => $_POST['code'],
                'adress' => $_POST['adress'],
                'firma' => $_POST['firma'],
                'fuel' => [
                    'fuel1' => $_POST['fuel1'],
                    'fuel2' => $_POST['fuel2'],
                    'fuel3' => $_POST['fuel3'],
                ],
                'price' => [
                    'a-95' => $_POST['a-95'],
                    'a-98' => $_POST['a-98'],
                    'dp' => $_POST['dp'],
                ],
                'litre' => $_POST['litre'],
            ];
        }
    }
}


$zapravkas = array_filter($zapravkas,function ($element){
    if($element['litre']<= 900){
        return true;
    }
    return false;
});

$searchedZapravka = null;
$searchedCode = $_GET['search_code'] ?? null;

if ($searchedCode !== null) {
    foreach ($zapravkas as $zapravka) {
        if ($zapravka['code'] == $searchedCode) {
            $searchedZapravka = $zapravka;
            break;
        }
    }
}

include 'templates/task.phtml';
include 'templates/form.phtml';
?>

<table border="1">
    <thead>
    <th>Code</th>
    <th>Adress</th>
    <th>Firma</th>
    <th>Litre</th>
    <th>Fuel</th>
    <th>Price</th>
    <th>Actions</th>
    </thead>
    <?php foreach ($zapravkas as $key => $zapravka): ?>
        <tr>
            <td><?= $zapravka['code']; ?></td>
            <td><?= $zapravka['adress']; ?></td>
            <td><?= $zapravka['firma']; ?></td>
            <td><?= $zapravka['litre']; ?></td>
            <td><?= implode(',', $zapravka['fuel']); ?></td>
            <td><?= implode(',', $zapravka['price']); ?></td>
            <td>

                <form method="POST" action="">
                    <input type="hidden" name="delete_index" value="<?= $key; ?>">
                    <button type="submit">Видалити</button>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<form method="GET" action="">
    <label for="search_code">Пошук за кодом:</label>
    <input type="text" id="search_code" name="search_code">
    <button type="submit">Пошук</button>
</form>
<?php if ($searchedZapravka !== null): ?>
    <h2>Результат пошуку:</h2>
    <table border="1">
        <thead>
        <th>Code</th>
        <th>Adress</th>
        <th>Firma</th>
        <th>Litre</th>
        <th>Fuel</th>
        <th>Price</th>
        <th>Actions</th>
        </thead>
        <tr>
            <td><?= $searchedZapravka['code']; ?></td>
            <td><?= $searchedZapravka['adress']; ?></td>
            <td><?= $searchedZapravka['firma']; ?></td>
            <td><?= $searchedZapravka['litre']; ?></td>
            <td><?= implode(',', $searchedZapravka['fuel']); ?></td>
            <td><?= implode(',', $searchedZapravka['price']); ?></td>
            <td>
                <form method="POST" action="">
                    <input type="hidden" name="edited_index" value="<?= $searchedZapravka['code']; ?>">
                    <button type="submit">Редагувати</button>
                </form>
            </td>
        </tr>
    </table>
<?php endif; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Редагування об'єкта</title>
</head>
<body>
<h1>Редагування об'єкта</h1>
<form method="post" action="">
    <input type="hidden" name="edited_index" value="<?= $editedIndex; ?>">

    <label for="code">Код:</label>
    <input type="text" id="code" name="code" value="<?= htmlspecialchars($editedZapravka['code'] ?? ''); ?>"><br>

    <label for="adress">Адреса:</label>
    <input type="text" id="adress" name="adress" value="<?= htmlspecialchars($editedZapravka['adress'] ?? ''); ?>"><br>

    <label for="firma">Фірма:</label>
    <input type="text" id="firma" name="firma" value="<?= htmlspecialchars($editedZapravka['firma'] ?? ''); ?>">
    <?php if (!empty($errors['firma'])): ?>
        <p class="error"><?= $errors['firma']; ?></p>
    <?php endif; ?>
    <br>

    <label for="fuel1">Паливо 1:</label>
    <input type="text" id="fuel1" name="fuel1" value="<?= htmlspecialchars($editedZapravka['fuel']['fuel1'] ?? ''); ?>"><br>

    <label for="fuel2">Паливо 2:</label>
    <input type="text" id="fuel2" name="fuel2" value="<?= htmlspecialchars($editedZapravka['fuel']['fuel2'] ?? ''); ?>"><br>

    <label for="fuel3">Паливо 3:</label>
    <input type="text" id="fuel3" name="fuel3" value="<?= htmlspecialchars($editedZapravka['fuel']['fuel3'] ?? ''); ?>"><br>

    <label for="a-95">Ціна (А-95):</label>
    <input type="text" id="a-95" name="a-95" value="<?= htmlspecialchars($editedZapravka['price']['a-95'] ?? ''); ?>"><br>

    <label for="a-98">Ціна (А-98):</label>
    <input type="text" id="a-98" name="a-98" value="<?= htmlspecialchars($editedZapravka['price']['a-98'] ?? ''); ?>"><br>

    <label for="dp">Ціна (ДП):</label>
    <input type="text" id="dp" name="dp" value="<?= htmlspecialchars($editedZapravka['price']['dp'] ?? ''); ?>"><br>

    <label for="litre">Літр:</label>
    <input type="text" id="litre" name="litre" value="<?= htmlspecialchars($editedZapravka['litre'] ?? ''); ?>">
    <?php if (!empty($errors['litre'])): ?>
        <p class="error"><?= $errors['litre']; ?></p>
    <?php endif; ?>
    <br>

    <button type="submit" name="submit">Зберегти зміни</button>
</form>
</body>
</html>